# Bit Scrambler

This program corrupts files. Used for emulating disk errors at a filesystem level. Currently only operates on filesystem files and not directly on block objects. Don't use on anything you love, as this will corrupt the file in a non-reversible way. 
