package main

import (
	"fmt"
	"math/rand"
	"os"
)

func main() {
	// 1 error for every errorInterval blocks
	errorInterval := 100
	// does not have to match that of the underlying filesystem,
	// currently affords no performance benefit because this is all random I/O
	// but using this to make my life easier in case I ever need it.
	blockSize := 512
	if len(os.Args) < 2 {
		panic(fmt.Errorf("not enough arguments. provide file name"))
	}
	fileName := os.Args[1]
	s, err := os.Stat(fileName)
	if err != nil {
		panic(err)
	}

	f, err := os.OpenFile(fileName, os.O_RDWR, s.Mode())
	if err != nil {
		panic(err)
	}
	defer f.Close()

	// which block number we're on
	var block int
	// how many bytes after the block boundary we're operating on
	var offset int
	// absolute value in bytes from the beginning of the file
	// block * blockSize + offset
	var location int

	for block*blockSize < int(s.Size()) {
		fileSizeLeft := int(s.Size()) - block*blockSize
		offset = rand.Intn(errorInterval * blockSize)
		if fileSizeLeft < errorInterval*blockSize {
			// we don't have a full error interval left,
			// add an error anyway, but not after the end of the file length

			offset = rand.Intn(fileSizeLeft)
		}
		location = block*blockSize + offset
		if location > int(s.Size()) {
			panic(fmt.Errorf("attempting to read past the end of the file, location: %x, but file size is %x", location, s.Size()))
		}
		var targetByte []byte = make([]byte, 1)
		n, err := f.ReadAt(targetByte, int64(location))
		if err != nil {
			panic(err)
		}
		if n != 1 {
			panic(fmt.Errorf("expected to read 1 byte, instead read %d", n))
		}
		if targetByte == nil {
			panic(fmt.Errorf("nothing read into memory"))
		}
		if len(targetByte) > 1 {
			panic(fmt.Errorf("byte slice is too large: Size is %d bytes", len(targetByte)))
		}

		// random bit position to flip
		bitToFlip := rand.Intn(7)
		bitMask := 1 >> bitToFlip
		if targetByte[0]&byte(bitMask) == 0 {
			// it's a zero bit, set it to one
			targetByte[0] = targetByte[0] | byte(bitMask)
		} else {
			// it's a one bit, set it to zero
			targetByte[0] = targetByte[0] ^ byte(bitMask)
		}

		// write the byte back

		if len(targetByte) > 1 {
			panic(fmt.Errorf("byte slice is too large: Size is %d bytes", len(targetByte)))
		}
		n, err = f.WriteAt(targetByte, int64(location))
		if err != nil {
			panic(err)
		}
		if n != 1 {
			panic(fmt.Errorf("expected to write 1 byte, instead wrote %d", n))
		}

		block += errorInterval
	}

}
